# Debug Voila Mybinder

Expérimentation du voila render via mybinder


- Url notebookRacine : https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.huma-num.fr%2Fmnauge%2Fdebug-voila-mybinder/HEAD?urlpath=voila%2Frender%2FnotebookRacine.ipynb


- url notebook dans un sous dossier : https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.huma-num.fr%2Fmnauge%2Fdebug-voila-mybinder/HEAD?urlpath=voila%2Frender%2Fnotebook%2FnotebookSubDir.ipynb

Attention dans le paramètre url qui permet de pointer vers le notebook à rendre par voilà il faut utiliser **urlpath** et non **labpath**